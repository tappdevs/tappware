<footer>
  <div class="footer_widget py-5">
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-sm-3">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>
        <div class="col-sm-3">
          <?php dynamic_sidebar('footer-3'); ?>
        </div>
        <div class="col-sm-3">
          <?php dynamic_sidebar('footer-4'); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          Copyright © 2019 TAPPWARE Solutions Limited
        </div>
        <div class="col-sm-6 d-flex justify-content-end">
          <?php wp_nav_menu(array('menu_class' => 'footer_menu','items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>','theme_location'=>'footer_menu')); ?>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
</body>
</html>
