<?php get_header(); ?>
<div class="content-heading py-3 bg-light">
  <div class="container">
    <div class="d-flex justify-content-between align-items-center">
      <h1>404 error!</h1>
      <ol class="breadcrumb mb-0 bg-transparent">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Library</a></li>
        <li class="breadcrumb-item active" aria-current="page">Data</li>
      </ol>
      <!-- /.breadcrumb -->
    </div>
    <!-- /.d-flex -->
  </div>
  <!-- /.container -->
</div>
<main>
  <div class="container py-5">

    <h2><?php _e( 'This is somewhat embarrassing, isn’t it?', 'tappware' ); ?></h2>
    <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'tappware' ); ?></p>

	  <?php get_search_form('header'); ?>

    <!-- /.row -->
  </div>
  <!-- /.container -->
</main>
<?php get_footer(); ?>
