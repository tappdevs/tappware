<?php
function custom_enqueue(){
//  wp_enqueue_style('reset-css', get_template_directory_uri() . '/css/reset.css', array(), '1.0.0', 'all' );
  wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.0.0', 'all' );
  wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '1.0.0', 'all' );
  wp_enqueue_style('jcarousel.responsive', get_template_directory_uri() . '/css/jcarousel.responsive.css', array(), '1.0.0', 'all' );


  wp_enqueue_style('media-style', get_template_directory_uri() . '/css/media.css', array(), '1.0.0', 'all' );
  wp_enqueue_style('custom-style', get_stylesheet_uri(), array(), '1.0.0', 'all' );

  wp_enqueue_script('jquery.jcarousel.min', get_template_directory_uri() . '/js/jquery.jcarousel.min.js', array(), '1.0.0', 'true' );
  wp_enqueue_script('jcarousel.responsive', get_template_directory_uri() . '/js/jcarousel.responsive.js', array(), '1.0.0', 'true' );
  wp_enqueue_script('isotope.pkgd.min', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array(), '1.0.0', 'true' );
  wp_enqueue_script('isotope.product', get_template_directory_uri() . '/js/isotope.product.js', array(), '1.0.0', 'true' );
  wp_enqueue_script('customjs', get_template_directory_uri() . '/js/tapp-script.js', array(), '1.0.0', 'true' );
}
add_action('wp_enqueue_scripts', 'custom_enqueue');
