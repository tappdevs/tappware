<?php


function create_post_type()
{
  register_post_type('project',
    array(
      'labels' => array(
        'name' => __('Projects'),
        'singular_name' => __('Project')
      ),
      'public' => true,
      'has_archive' => true,
      'query_var' => true,
      'menu_icon' => 'dashicons-category',
      'supports' => array('thumbnail', 'title', 'editor'),
    )
  );
}

add_action('init', 'create_post_type');

/*taxonomies*/

$tax_dept = array(
  'hierarchical' => true,
  'labels' => array(
    'name' => _x("Project Type", 'taxonomy general name', 'tappware'),
    'singular_name' => _x('Project Type', 'taxonomy singular name', 'tappware'),
    'search_items' => __('Search Types', 'tappware'),
    'all_items' => __('All Types', 'tappware'),
    'parent_item' => __('Parent Type', 'tappware'),
    'parent_item_colon' => __('Parent Type:', 'tappware'),
    'edit_item' => __('Edit Type', 'tappware'),
    'update_item' => __('Update Type', 'tappware'),
    'add_new_item' => __('Add New Type', 'tappware'),
    'new_item_name' => __('New Type Name', 'tappware'),
    'menu_name' => __('Project Types', 'tappware'),
  ),
  'show_ui' => true,
  'show_admin_column' => true,
  'query_var' => true,
  'rewrite' => array('slug' => 'type'),
);
register_taxonomy('project_type', 'project', $tax_dept);
