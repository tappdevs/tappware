<?php
if ( ! function_exists( 'tappware_register_nav_menu' ) ) {

	function tappware_register_nav_menu() {
		register_nav_menus( array(
			'primary_menu' => __( 'Primary Menu', 'tappware' ),
			'footer_menu'  => __( 'Footer Menu', 'tappware' ),
		) );
	}

	add_action( 'after_setup_theme', 'tappware_register_nav_menu', 0 );
}
function pr( $var ) {
	echo '<pre>';
	print_r( $var );
	echo '</pre>';
}

add_theme_support( 'post-thumbnails' );

add_action( 'widgets_init', 'tappware_slug_widgets_init' );
function tappware_slug_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Page Sidebar', 'tappware' ),
		'id'            => 'sidebar-page',
		'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widgettitle mb-3">',
		'after_title'   => '</h5>',
	) );
	register_sidebar( array(
		'name'          => __( 'Content Gallery', 'tappware' ),
		'id'            => 'content-gallery',
		'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="mb-4">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Contact Sidebar', 'tappware' ),
		'id'            => 'sidebar-contact',
		'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="content-title"><span>',
		'after_title'   => '</span></h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer One', 'tappware' ),
		'id'            => 'footer-1',
		'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle mb-3">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Two', 'tappware' ),
		'id'            => 'footer-2',
		'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle mb-3">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Three', 'tappware' ),
		'id'            => 'footer-3',
		'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle mb-3">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Four', 'tappware' ),
		'id'            => 'footer-4',
		'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle mb-3">',
		'after_title'   => '</h4>',
	) );
}
