<?php /*Template name: Services*/ ?>
<?php get_header(); ?>
<div class="content-heading py-3 bg-light">
  <div class="container">
    <div class="d-flex justify-content-between align-items-center">
      <h1><?php the_title(); ?></h1>
      <?php yoast_breadcrumb('<div id="breadcrumbs">', '</div>'); ?>
      <!-- /.breadcrumb -->
    </div>
    <!-- /.d-flex -->
  </div>
  <!-- /.container -->
</div>
<main>

  <div class="service-feature border-bottom">
    <div class="container py-5">
      <div class="row">
        <div class="col-sm-4">
          <img src="<?= get_template_directory_uri() ?>/images/sf1.png" alt="feature">
          <h4 class="my-2">Quality Assurance team </h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad corporis deserunt doloribus earum eius error
            est explicabo, in ipsam labore magni molestiae perferendis quia quibusdam, reiciendis repellat saepe sequi
            vero?</p>
        </div>
        <div class="col-sm-4">
          <img src="<?= get_template_directory_uri() ?>/images/sf2.png" alt="feature">
          <h4 class="my-2">Quality Assurance team </h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad corporis deserunt doloribus earum eius error
            est explicabo, in ipsam labore magni molestiae perferendis quia quibusdam, reiciendis repellat saepe sequi
            vero?</p>
        </div>
        <div class="col-sm-4">
          <img src="<?= get_template_directory_uri() ?>/images/sf3.png" alt="feature">
          <h4 class="my-2">Quality Assurance team </h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad corporis deserunt doloribus earum eius error
            est explicabo, in ipsam labore magni molestiae perferendis quia quibusdam, reiciendis repellat saepe sequi
            vero?</p>


        </div>
      </div>

    </div>
  </div>

  <div class="service-core bg-light border-bottom">
    <div class="container py-5">
      <div class="row">
        <div class="col-sm-6">
          <div class="mb-5">
            <h4 class="mb-3 content-title"><span class="bg-light">We are great at what we do</span></h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur assumenda consequuntur cumque
              deserunt ea earum excepturi illum, iusto modi molestiae quos suscipit voluptatum. Aperiam asperiores
              doloremque, facere molestias omnis possimus.</p>
          </div>


          <div>
            <h4 class="mb-3 content-title"><span class="bg-light">Our Experience</span></h4>
            <div class="mb-3">
              <span>Creative Service - 12 years</span>
              <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 94%;" aria-valuenow="94"
                    aria-valuemin="0" aria-valuemax="100">94%
                </div>
              </div>
            </div>
            <div class="mb-3">
              <span>Creative Service - 12 years</span>
              <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 29%;" aria-valuenow="29"
                    aria-valuemin="0" aria-valuemax="100">29%
                </div>
              </div>
            </div>
            <div class="mb-3">
              <span>Creative Service - 12 years</span>
              <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 78%;" aria-valuenow="78"
                    aria-valuemin="0" aria-valuemax="100">78%
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <h4 class="mb-3 content-title"><span class="bg-light">The core service we provide</span></h4>
          <div class="d-flex mb-2">
            <div>
              <span class="fa-stack fa-lg text-center">
              <i class="fa fa-circle font-gradient fa-stack-2x"></i>
              <i class="fa fa-list fa-stack-1x fa-inverse"></i>
            </span>
            </div>
            <div class="ml-2">
              <h5 class="font-weight-bold">Conception</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore eaque ipsa molestias
                provident sed voluptates.</p>
            </div>
          </div>

          <div class="d-flex mb-2">
            <div>
              <span class="fa-stack fa-lg text-center">
              <i class="fa fa-circle font-gradient fa-stack-2x"></i>
              <i class="fa fa-book fa-stack-1x fa-inverse"></i>
            </span>
            </div>
            <div class="ml-2">
              <h5 class="font-weight-bold">Wireframing</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore eaque ipsa molestias
                provident sed voluptates.</p>
            </div>
          </div>

          <div class="d-flex mb-2">
            <div>
              <span class="fa-stack fa-lg text-center">
              <i class="fa fa-circle font-gradient fa-stack-2x"></i>
              <i class="fa fa-calendar fa-stack-1x fa-inverse"></i>
            </span>
            </div>
            <div class="ml-2">
              <h5 class="font-weight-bold">Designing</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore eaque ipsa molestias
                provident sed voluptates.</p>
            </div>
          </div>
          <div class="d-flex mb-2">
            <div>
              <span class="fa-stack fa-lg text-center">
              <i class="fa fa-circle font-gradient fa-stack-2x"></i>
              <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
            </span>
            </div>
            <div class="ml-2">
              <h5 class="font-weight-bold">Coding</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore eaque ipsa molestias
                provident sed voluptates.</p>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>

  <div class="service-check">
    <div class="container py-5">

      <div class="row">
        <div class="col-sm-8">
          <h4 class="mb-3 content-title"><span>Checkout our Services!</span></h4>
          <p class="font-weight-light font-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci
            dolore numquam</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam blanditiis, commodi corporis cupiditate
            dicta error et incidunt ipsam, laudantium modi nam necessitatibus pariatur perspiciatis saepe, tenetur ut
            voluptates voluptatum? Deserunt.</p>

          <div class="row">
            <div class="col-sm-6">
              <div class="d-flex mb-3">
                <i class="fa fa-pencil font-gradient fa-2x"></i>
                <div class="ml-3">
                  <h5 class="font-weight-bold">Coding</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="d-flex mb-3">
                <i class="fa fa-list font-gradient fa-2x"></i>
                <div class="ml-3">
                  <h5 class="font-weight-bold">Coding</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="d-flex mb-3">
                <i class="fa fa-cogs font-gradient fa-2x"></i>
                <div class="ml-3">
                  <h5 class="font-weight-bold">Coding</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="d-flex mb-3">
                <i class="fa fa-gavel font-gradient fa-2x"></i>
                <div class="ml-3">
                  <h5 class="font-weight-bold">Coding</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="d-flex mb-3">
                <i class="fa fa-check-square-o font-gradient fa-2x"></i>
                <div class="ml-3">
                  <h5 class="font-weight-bold">Coding</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="d-flex mb-3">
                <i class="fa fa-area-chart font-gradient fa-2x"></i>
                <div class="ml-3">
                  <h5 class="font-weight-bold">Coding</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-4">

          <img src="<?= get_template_directory_uri() ?>/images/sc1.png" alt="feature">
          <br>
          <br>
          <img src="<?= get_template_directory_uri() ?>/images/sc1.png" alt="feature">


        </div>

      </div>
    </div>
  </div>


</main>
<?php get_footer(); ?>
