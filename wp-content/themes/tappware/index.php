<?php get_header(); ?>
<div class="content-heading py-3 bg-light">
	<div class="container">
		<div class="d-flex justify-content-between align-items-center">
			<h1><?php the_title(); ?></h1>
      <?php yoast_breadcrumb('<div id="breadcrumbs">', '</div>'); ?>
			<!-- /.breadcrumb -->
		</div>
		<!-- /.d-flex -->
	</div>
	<!-- /.container -->
</div>
<main>
	<div class="container py-5">
		<div class="row">
			<div class="col-sm-9">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						//
						the_content();
						//
					} // end while
				} // end if
				?>
			</div>
			<!-- /.col-sm-9 -->
			<?php get_sidebar(); ?>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</main>
<?php get_footer(); ?>
