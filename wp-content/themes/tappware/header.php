<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?= wp_title( '' ); ?></title>
  <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800|Oswald:400,600|Barlow:300,400,500,700&display=swap"
      rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body <?php body_class() ?>>
<header class="shadow">
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="header-top-left">
            <a href="tel:01712151416" class="mr-2"><i class="fa fa-phone mr-1"></i> 01712105016</a>
            <a href="https://goo.gl/maps/YFZ46ME2RW79FaiCA" target="_blank"><i class="fa fa-map-marker mr-1"></i> 566
              (2nd floor), Road 8, Avenue 3, Mirpur DOHS</a>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="header-top-right text-right">
            <div class="social">
              <a href="http://facebook.com"><i class="fa fa-facebook-square"></i></a>
              <a href="http://facebook.com"><i class="fa fa-instagram"></i></a>
              <a href="http://facebook.com"><i class="fa fa-youtube-square"></i></a>
              <a href="http://facebook.com"><i class="fa fa-twitter-square"></i></a>
              <a href="http://facebook.com"><i class="fa fa-linkedin-square"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="header">
    <div class="container position-relative">
      <div class="row align-items-center">
        <div class="col-sm-6">
          <div class="logo">
            <a href="<?= home_url() ?>"><img src="<?= get_template_directory_uri() ?>/images/logo.png" alt="logo"></a>
          </div>
        </div>
        <div class="col-sm-6 d-flex align-items-center justify-content-xl-end">
			<?php wp_nav_menu( array( 'menu_class'     => 'main_menu',
			                          'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			                          'theme_location' => 'primary_menu'
			) ); ?>
          <button class="fa fa-search btn btn-transparent p-0 ml-3 text-primary search-toggler"></button>
        </div>
      </div>
		<?php
			get_search_form();
		?>
    </div>
  </div>
</header>
