<?php /*Template name: Products*/ ?>
<?php get_header(); ?>

<div class="content-heading py-3 bg-light">
  <div class="container">
    <div class="d-flex justify-content-between align-items-center">
      <h1><?php the_title(); ?></h1>
      <?php yoast_breadcrumb('<div id="breadcrumbs">', '</div>'); ?>
      <!-- /.breadcrumb -->
    </div>
    <!-- /.d-flex -->
  </div>
  <!-- /.container -->
</div>
<main>


  <div class="product-grid py-5">
    <div class="container">
      <?php
      $project_cats = get_terms(array(
        'taxonomy' => 'project_type',
        'orderby' => 'id',
      ));
      ?>
      <div class="product-categories mb-3">
        <button class="button active" data-filter="*"><i class="fa fa-list"></i> Show all</button>
        <?php foreach ($project_cats as $project_cat) { ?>
          <button class="button" data-filter=".<?= $project_cat->slug ?>"><i
                class="fa fa-list"></i> <?= $project_cat->name ?>
          </button>
        <?php } ?>
      </div>

      <div class="product-list">
        <div class="row grid">
          <?php
          $project_args = array(
            'showposts' => -1,
            'post_type' => 'project'
          );

          $projects = get_posts($project_args);

          foreach ($projects as $project) {
            $project_types = join(' ', wp_list_pluck(get_the_terms($project->ID, 'project_type'), 'slug'));
            $project_thumb = get_the_post_thumbnail_url($project->ID, 'full');
            ?>
            <div class="col-sm-3 mb-4 grid-item  <?= $project_types ?>">
              <div class="card">
                <div class="card-image">
                  <img class="card-img-top" src="<?= $project_thumb ?>"
                      alt="feature">
                  <div class="card-links d-flex justify-content-center align-items-center">
                    <a class="noline btn btn-sm btn-outline-success" rel="lightbox"
                        href="<?= $project_thumb ?>"><i
                          class="fa fa-eye"></i></a>
                    <a class="noline btn btn-sm btn-outline-info"
                        href="<?= get_post_permalink($project->ID) ?>"><i class="fa fa-link"></i></a>
                  </div>
                </div>
                <div class="card-body text-center">
                  <h5 class="card-title"><?= $project->post_title ?></h5>
                  <p class="card-text"><?= $project->post_content ?></p>
                </div>
              </div>
            </div>

            <?php
          }
          ?>

        </div>

      </div>

    </div>
  </div>


  <div class="product-carousel py-5 bg-light">
    <div class="container">

      <div class="block-heading text-center">
        <h2 class="oswald">About Founders</h2>
        <p class="font-italic">We bring you powerful advantages to navigate your digital transformation</p>
      </div>


      <div class="jcarousel-wrapper">
        <div class="jcarousel">
          <ul>
            <li><img src="<?= get_template_directory_uri() ?>/images/client1.png" alt="Image 1"></li>
            <li><img src="<?= get_template_directory_uri() ?>/images/client2.png" alt="Image 2"></li>
            <li><img src="<?= get_template_directory_uri() ?>/images/client3.png" alt="Image 3"></li>
            <li><img src="<?= get_template_directory_uri() ?>/images/client4.png" alt="Image 4"></li>
            <li><img src="<?= get_template_directory_uri() ?>/images/client5.png" alt="Image 5"></li>
            <li><img src="<?= get_template_directory_uri() ?>/images/client6.png" alt="Image 6"></li>
          </ul>
        </div>
        <!--<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
        <a href="#" class="jcarousel-control-next">&rsaquo;</a>

        <p class="jcarousel-pagination"></p>-->

      </div>
    </div>
  </div>


</main>
<?php get_footer(); ?>
