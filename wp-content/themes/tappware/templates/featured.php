<section class="featured py-5 text-white" style="background: #0b78bc;
background: -moz-linear-gradient(45deg,  #0b78bc 0%, #009afb 100%);
background: -webkit-linear-gradient(45deg,  #0b78bc 0%,#009afb 100%);
background: linear-gradient(45deg,  #0b78bc 0%,#009afb 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0b78bc', endColorstr='#009afb',GradientType=1 );
">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-6">
				<h5>Always-on learning</h5>
				<p>Continuous improvement through transferring digital skills and ideas</p>
				<a href="#" class="btn-anim-side">Explore</a>
				<div class="post_block row pt-4">
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 text-right">
				<img src="<?= get_template_directory_uri(); ?>/images/feature1.png" alt="feature1">
			</div>
		</div>
	</div>
</section>
<section class="featured py-5 text-white" style="background: #963596;
background: -moz-linear-gradient(45deg,  #963596 1%, #ea08ea 100%);
background: -webkit-linear-gradient(45deg,  #963596 1%,#ea08ea 100%);
background: linear-gradient(45deg,  #963596 1%,#ea08ea 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#963596', endColorstr='#ea08ea',GradientType=1 );
">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-6 text-left">
				<img src="<?= get_template_directory_uri(); ?>/images/feature2.png" alt="feature2">
			</div>
			<div class="col-sm-6">
				<h5>Always-on learning</h5>
				<p>Continuous improvement through transferring digital skills and ideas</p>
				<a href="#" class="btn-anim-side">Explore</a>
				<div class="post_block row pt-4">
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="featured py-5 text-white" style="background: #0ac171;
background: -moz-linear-gradient(45deg,  #0ac171 0%, #06dc7f 100%);
background: -webkit-linear-gradient(45deg,  #0ac171 0%,#06dc7f 100%);
background: linear-gradient(45deg,  #0ac171 0%,#06dc7f 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0ac171', endColorstr='#06dc7f',GradientType=1 );
">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-6">
				<h5>Always-on learning</h5>
				<p>Continuous improvement through transferring digital skills and ideas</p>
				<a href="#" class="btn-anim-side">Explore</a>
				<div class="post_block row pt-4">
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="post mb-3">
							<p class="font-weight-bold">Improving productivity with digital field workforce management </p>
							<a href="#" class="text-uppercase btn-view">view</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 text-right">
				<img src="<?= get_template_directory_uri(); ?>/images/feature3.png" alt="feature3">
			</div>
		</div>
	</div>
</section>
