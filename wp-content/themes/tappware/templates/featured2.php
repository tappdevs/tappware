<section class="featured py-5 bg-secondary text-light">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-3 mb-3">
        <div class="exp-item text-center">
          <span class="fa-stack fa-3x">
            <i class="fa fa-circle fa-stack-2x text-warning"></i>
            <i class="fa fa-flag fa-stack-1x fa-inverse"></i>
          </span>
          <h5 class="my-3">12 YEARS OF EXPERIENCE</h5>
          <p>Because diversity lorem ipsum dolor porta ullam rutrum glavrida lorem unicus amet.</p>
        </div>
      </div>
      <!-- /.col-sm-3 -->
      <div class="col-sm-3 mb-3">
        <div class="exp-item text-center">
          <span class="fa-stack fa-3x">
            <i class="fa fa-circle fa-stack-2x text-warning"></i>
            <i class="fa fa-flag fa-stack-1x fa-inverse"></i>
          </span>
          <h5 class="my-3">12 YEARS OF EXPERIENCE</h5>
          <p>Because diversity lorem ipsum dolor porta ullam rutrum glavrida lorem unicus amet.</p>
        </div>
      </div>
      <!-- /.col-sm-3 -->
      <div class="col-sm-3 mb-3">
        <div class="exp-item text-center">
          <span class="fa-stack fa-3x">
            <i class="fa fa-circle fa-stack-2x text-warning"></i>
            <i class="fa fa-flag fa-stack-1x fa-inverse"></i>
          </span>
          <h5 class="my-3">12 YEARS OF EXPERIENCE</h5>
          <p>Because diversity lorem ipsum dolor porta ullam rutrum glavrida lorem unicus amet.</p>
        </div>
      </div>
      <!-- /.col-sm-3 -->
      <div class="col-sm-3 mb-3">
        <div class="exp-item text-center">
          <span class="fa-stack fa-3x">
            <i class="fa fa-circle fa-stack-2x text-warning"></i>
            <i class="fa fa-flag fa-stack-1x fa-inverse"></i>
          </span>
          <h5 class="my-3">12 YEARS OF EXPERIENCE</h5>
          <p>Because diversity lorem ipsum dolor porta ullam rutrum glavrida lorem unicus amet.</p>
        </div>
      </div>
      <!-- /.col-sm-3 -->

    </div>
    <!-- /.row -->
  </div>
</section>
