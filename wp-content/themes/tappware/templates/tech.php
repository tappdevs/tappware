<section class="featured py-5">
  <div class="block-heading text-center">
    <h2 class="oswald">AI-powered core</h2>
    <p class="font-italic">We bring you powerful advantages to navigate your digital transformation</p>
  </div>
  <div class="image-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-2">
          <div class="image-card bor-dash-blue" style="background-color:#fffcfc;">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client1.png" alt="Card image cap">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="image-card bor-dash-blue" style="background-color:#f4fcfd">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client2.png" alt="Card image cap">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="image-card bor-dash-blue alert-success">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client3.png" alt="Card image cap">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="image-card bor-dash-blue alert-danger">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client3.png" alt="Card image cap">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="image-card bor-dash-blue alert-warning">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client4.png" alt="Card image cap">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="image-card bor-dash-blue alert-info">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client4.png" alt="Card image cap">
            </div>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="image-card bor-dash-blue alert-light">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client1.png" alt="Card image cap">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="image-card bor-dash-blue alert-dark">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client2.png" alt="Card image cap">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
