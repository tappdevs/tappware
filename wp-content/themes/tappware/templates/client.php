<section class="featured py-5 alert-info">
  <div class="block-heading text-center">
    <h2 class="oswald">AI-powered core</h2>
    <p class="font-italic">We bring you powerful advantages to navigate your digital transformation</p>
  </div>
  <div class="image-block">
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="image-card bor-dash-blue bg-white">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client1.png" alt="Card image cap">
            </div>
            <h6 class="card-title"><a href="#">Government of the People's Republic of Bangladesh</a>
            </h6>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="image-card bor-dash-blue bg-white">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client2.png" alt="Card image cap">
            </div>
            <h6 class="card-title"><a href="#">Community And People Support</a></h6>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="image-card bor-dash-blue bg-white">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client3.png" alt="Card image cap">
            </div>
            <h6 class="card-title"><a href="#">Save the Children</a></h6>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="image-card bor-dash-blue bg-white">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client3.png" alt="Card image cap">
            </div>
            <h6 class="card-title"><a href="#">Save the Children</a></h6>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="image-card bor-dash-blue bg-white">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client4.png" alt="Card image cap">
            </div>
            <h6 class="card-title"><a href="#">Skills for Employment Investment Program</a></h6>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="image-card bor-dash-blue bg-white">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client4.png" alt="Card image cap">
            </div>
            <h6 class="card-title"><a href="#">Skills for Employment Investment Program</a></h6>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="image-card bor-dash-blue bg-white">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client1.png" alt="Card image cap">
            </div>
            <h6 class="card-title"><a href="#">Government of the People's Republic of Bangladesh</a>
            </h6>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="image-card bor-dash-blue bg-white">
            <div class="img-wrap">
              <img src="<?= get_template_directory_uri() ?>/images/client2.png" alt="Card image cap">
            </div>
            <h6 class="card-title"><a href="#">Community And People Support</a></h6>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
