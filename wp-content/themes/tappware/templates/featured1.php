<section class="featured py-5 ">
  <div class="block-heading text-center">
    <h2 class="oswald">AI-powered core</h2>
    <p class="font-italic">We bring you powerful advantages to navigate your digital transformation</p>
  </div>
  <div class="hover-block">
    <div class="container">
      <div class="row no-gutters">
        <div class="col-6 hover-wrap">
          <img src="<?= get_template_directory_uri(); ?>/images/f1.jpg" alt="feature2">
          <div class="hover-content">
            <div class="cont-wrap">
              <h4 class="mb-3">Experience</h4>
              <p class="font-italic">Design-led transformation. From brand to experience. </p>
              <p>Design-led transformation. From brand to experienceDesign-led transformation. experienceDesign-led transformation. From brand to transformation</p>
            </div>
          </div>
        </div>
        <div class="col-6 row no-gutters">
          <div class="col-6 hover-wrap">
            <img src="<?= get_template_directory_uri(); ?>/images/f2.jpg" alt="feature2">
          <div class="hover-content">
            <div class="cont-wrap">
              <h4 class="mb-3">Experience</h4>
              <p class="font-italic">Design-led transformation. From brand to experience. </p>
              <p>Design-led transformation. From brand to experienceDesign-led transformation. experienceDesign-led transformation. From brand to transformation</p>
            </div>
          </div>
          </div>
          <div class="col-6 hover-wrap">
            <img src="<?= get_template_directory_uri(); ?>/images/f3.jpg" alt="feature2">
          <div class="hover-content">
            <div class="cont-wrap">
              <h4 class="mb-3">Experience</h4>
              <p class="font-italic">Design-led transformation. From brand to experience. </p>
              <p>Design-led transformation. From brand to experienceDesign-led transformation. experienceDesign-led transformation. From brand to transformation</p>
            </div>
          </div>
          </div>
          <div class="col-6 hover-wrap">
            <img src="<?= get_template_directory_uri(); ?>/images/f4.jpg" alt="feature2">
          <div class="hover-content">
            <div class="cont-wrap">
              <h4 class="mb-3">Experience</h4>
              <p class="font-italic">Design-led transformation. From brand to experience. </p>
              <p>Design-led transformation. From brand to experienceDesign-led transformation. experienceDesign-led transformation. From brand to transformation</p>
            </div>
          </div>
          </div>
          <div class="col-6 hover-wrap">
            <img src="<?= get_template_directory_uri(); ?>/images/f5.jpg" alt="feature2">
          <div class="hover-content">
            <div class="cont-wrap">
              <h4 class="mb-3">Experience</h4>
              <p class="font-italic">Design-led transformation. From brand to experience. </p>
              <p>Design-led transformation. From brand to experienceDesign-led transformation. experienceDesign-led transformation. From brand to transformation</p>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
