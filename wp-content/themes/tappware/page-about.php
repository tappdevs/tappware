<?php /*Template name: About */ ?>
<?php get_header(); ?>
<div class="content-heading py-3 bg-light">
  <div class="container">
    <div class="d-flex justify-content-between align-items-center">
      <h1><?php the_title(); ?></h1>
      <?php yoast_breadcrumb('<div id="breadcrumbs">', '</div>'); ?>
      <!-- /.breadcrumb -->
    </div>
    <!-- /.d-flex -->
  </div>
  <!-- /.container -->
</div>
<main>
  <div class="inner-banner">
    <img src="<?= get_template_directory_uri() ?>/images/inner-banner.jpg" alt="banner">
  </div>
  <div class="container py-5">
    <div class="row">
      <div class="col-sm-7">
        <h4>Overview</h4>
        <p>Suspendisse ullamcorper finibus justo eget auctor. Vestibulum ligula orci, volutpat id aliquet eget,
          consectetur eget ante.</p>

        <p>Nam eget neque pellentesque, efficitur neque at, ornare orci. Morbi convallis a eros fermentum rhoncus. Morbi
          convallis a eros fermentum rhoncus lorem. Vestibulum ligula orci, volutpat id aliquet eget, consectetur eget
          ante. Duis pharetra for nec rhoncus felis sagittis nec amet ultricies lorem.</p>

        <p>Quisque lorem 12 YEARS quis efficitur felis. Duis pharetra 86 CLIENTS for amet ultricies augue, nec rhoncus
          felis 7 AWARDS sagittis nec.</p>
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-5">
        <h4>Our Experience</h4>
        <div class="mb-3">
          <span>Creative Service - 12 years</span>
          <div class="progress">
            <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 94%;" aria-valuenow="94"
                aria-valuemin="0" aria-valuemax="100">94%
            </div>
          </div>
        </div>
        <div class="mb-3">
          <span>Creative Service - 12 years</span>
          <div class="progress">
            <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 29%;" aria-valuenow="29"
                aria-valuemin="0" aria-valuemax="100">29%
            </div>
          </div>
        </div>
        <div class="mb-3">
          <span>Creative Service - 12 years</span>
          <div class="progress">
            <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 78%;" aria-valuenow="78"
                aria-valuemin="0" aria-valuemax="100">78%
            </div>
          </div>
        </div>
        <div class="mb-3">
          <span>Creative Service - 12 years</span>
          <div class="progress">
            <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 86%;" aria-valuenow="86"
                aria-valuemin="0" aria-valuemax="100">86%
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.containeer -->
  <!-- /.inner-banner -->
  <!-- /.container -->

  <div class="specialities bg-light py-3">
    <div class="container">

      <div class="row">
        <div class="col-sm-4 d-flex align-items-center">
          <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x font-gradient"></i>
            <i class="fa fa-flag fa-stack-1x fa-inverse"></i>
          </span>
          <div class="ml-2">
            <strong class="has-huge-font-size">16</strong>
            <p class="mb-0">Specialists</p>
          </div>
        </div>
        <div class="col-sm-4 d-flex align-items-center">
          <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x font-gradient"></i>
            <i class="fa fa-flag fa-stack-1x fa-inverse"></i>
          </span>
          <div class="ml-2">
            <strong class="has-huge-font-size">16</strong>
            <p class="mb-0">Specialists</p>
          </div>
        </div>
        <div class="col-sm-4 d-flex align-items-center">
          <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x font-gradient"></i>
            <i class="fa fa-flag fa-stack-1x fa-inverse"></i>
          </span>
          <div class="ml-2">
            <strong class="has-huge-font-size">16</strong>
            <p class="mb-0">Specialists</p>
          </div>
        </div>
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
  </div>
  <!-- /.specialities -->

  <div class="featured-list-gallery py-5">
    <div class="container">
      <div class="row">
        <div class="col-sm-7">
          <h4 class="mb-4">Principles of our work</h4>
          <div class="d-flex">
            <div>
              <span class="fa-stack fa-lg text-center">
              <i class="fa fa-circle-thin font-gradient fa-stack-2x"></i>
              <strong class="has-regular-font-size align-bottom">1</strong>
            </span>
            </div>
            <div class="ml-2">
              <h5 class="font-weight-bold">Creativity</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore eaque ipsa molestias
                provident sed voluptates.</p>
            </div>
          </div>
          <div class="d-flex mb-2">
            <div>
              <span class="fa-stack fa-lg text-center">
              <i class="fa fa-circle-thin font-gradient fa-stack-2x"></i>
              <strong class="has-regular-font-size align-bottom">2</strong>
            </span>
            </div>
            <div class="ml-2">
              <h5 class="font-weight-bold">Creativity</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore eaque ipsa molestias
                provident sed voluptates.</p>
            </div>
          </div>
          <div class="d-flex mb-2">
            <div>
              <span class="fa-stack fa-lg text-center">
              <i class="fa fa-circle-thin font-gradient fa-stack-2x"></i>
              <strong class="has-regular-font-size align-bottom">3</strong>
            </span>
            </div>
            <div class="ml-2">
              <h5 class="font-weight-bold">Creativity</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore eaque ipsa molestias
                provident sed voluptates.</p>
            </div>
          </div>
          <div class="d-flex mb-2">
            <div>
              <span class="fa-stack fa-lg text-center">
              <i class="fa fa-circle-thin font-gradient fa-stack-2x"></i>
              <strong class="has-regular-font-size align-bottom">4</strong>
            </span>
            </div>
            <div class="ml-2">
              <h5 class="font-weight-bold">Creativity</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore eaque ipsa molestias
                provident sed voluptates.</p>
            </div>
          </div>
        </div>
        <div class="col-sm-5">
			<?php dynamic_sidebar( 'content-gallery' ) ?>
        </div>
      </div>
    </div>
  </div>

  <div class="founders py-5 bg-light">
    <div class="block-heading text-center">
      <h2 class="oswald">About Founders</h2>
      <p class="font-italic">We bring you powerful advantages to navigate your digital transformation</p>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="row align-items-start">
            <div class="col-sm-5">
              <img class="rounded-circle border border-danger"
                  src="<?= get_template_directory_uri() ?>/images/director.jpg"
                  alt="Kamruzzaman Niton">
            </div>
            <div class="col-sm-7">
              <div class="mb-3">
                <h4 class="barlow mb-0">Kamruzzaman Niton</h4>
                <strong class="barlow">President</strong>
              </div>
              <div class="mb-3">
                <p class="mb-0"><a href="mailto:niton@tappware.com"><i class="fa fa-envelope"></i>
                    niton@tappware.com</a></p>
                <p class="mb-0"><a href="skype:zaman.niton"><i class="fa fa-skype"></i> zaman.niton</a></p>
              </div>
              <div class="content-social d-flex mb-3">
                <a href="#">
                  <span><i class="fa fa-facebook"></i></span>
                </a>
                <a href="#">
                  <span><i class="fa fa-twitter"></i></span>
                </a>
                <a href="#">
                  <span><i class="fa fa-instagram"></i></span>
                </a>
                <a href="#">
                  <span><i class="fa fa-linkedin"></i></span>
                </a>
              </div>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto, atque consectetur
                consequatur dicta dolorem eaque eligendi, illum minima nesciunt obcaecati possimus </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row align-items-start">
            <div class="col-sm-5">
              <img class="rounded-circle border border-warning" src="<?= get_template_directory_uri() ?>/images/md.jpg"
                  alt="Kamruzzaman Niton">
            </div>
            <div class="col-sm-7">
              <div class="mb-3">
                <h4 class="barlow mb-0">Shampa Nasrin</h4>
                <strong class="barlow">Managing Director</strong>
              </div>
              <div class="mb-3">
                <p class="mb-0"><a href="mailto:nasrin@tappware.com"><i class="fa fa-envelope"></i>
                    nasrin@tappware.com</a></p>
                <p class="mb-0"><a href="skype:shampa0715"><i class="fa fa-skype"></i> shampa0715</a></p>
              </div>
              <div class="content-social d-flex mb-3">
                <a href="#">
                  <span><i class="fa fa-facebook"></i></span>
                </a>
                <a href="#">
                  <span><i class="fa fa-twitter"></i></span>
                </a>
                <a href="#">
                  <span><i class="fa fa-instagram"></i></span>
                </a>
                <a href="#">
                  <span><i class="fa fa-linkedin"></i></span>
                </a>
              </div>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto, atque consectetur
                consequatur dicta dolorem eaque eligendi, illum minima nesciunt obcaecati possimus </p>
            </div>
          </div>
        </div>
      </div>

      <hr class="mt-5">

      <div class="block-heading text-center my-5">
        <h2 class="oswald">Other Members</h2>
      </div>

      <div class="row justify-content-center">
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/dolar.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jafar Al Farhad</h6>
              <p class="barlow mb-0">System Administrator</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/hasan.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Hasan Tareq</h6>
              <p class="barlow mb-0">Sr. Software Engineer</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/niloy.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jubaer Bin Ali</h6>
              <p class="barlow mb-0">Jr Software Engineer</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/dolar.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jafar Al Farhad</h6>
              <p class="barlow mb-0">President</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/dolar.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jafar Al Farhad</h6>
              <p class="barlow mb-0">President</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/dolar.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jafar Al Farhad</h6>
              <p class="barlow mb-0">President</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/dolar.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jafar Al Farhad</h6>
              <p class="barlow mb-0">President</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/dolar.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jafar Al Farhad</h6>
              <p class="barlow mb-0">President</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/dolar.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jafar Al Farhad</h6>
              <p class="barlow mb-0">President</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>
        <div class="col-sm-2 mb-3">
          <div class="team-member bg-white p-4 text-center">
            <img class="rounded-circle border border-light" src="<?= get_template_directory_uri() ?>/images/dolar.jpg"
                alt="Kamruzzaman Niton">
            <div class="mt-3">
              <h6 class="barlow mb-0">Abu Jafar Al Farhad</h6>
              <p class="barlow mb-0">President</p>
            </div>
            <div class="content-social d-flex my-2 justify-content-center">
              <a href="#">
                <span><i class="fa fa-facebook"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-twitter"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-instagram"></i></span>
              </a>
              <a href="#">
                <span><i class="fa fa-linkedin"></i></span>
              </a>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="clients py-5">

    <div class="block-heading text-center">
      <h2 class="oswald">Proud to work with</h2>
    </div>

    <div class="clients-images">
      <div class="container">
        <div class="jcarousel-wrapper">
          <div class="jcarousel">
            <ul>
              <li><img src="<?= get_template_directory_uri() ?>/images/client1.png" alt="Image 1"></li>
              <li><img src="<?= get_template_directory_uri() ?>/images/client2.png" alt="Image 2"></li>
              <li><img src="<?= get_template_directory_uri() ?>/images/client3.png" alt="Image 3"></li>
              <li><img src="<?= get_template_directory_uri() ?>/images/client4.png" alt="Image 4"></li>
              <li><img src="<?= get_template_directory_uri() ?>/images/client5.png" alt="Image 5"></li>
              <li><img src="<?= get_template_directory_uri() ?>/images/client6.png" alt="Image 6"></li>
            </ul>
          </div>

        </div>


        <!--<div class="d-flex justify-content-around align-items-center flex-wrap">
          <img src="<?/*= get_template_directory_uri() */?>/images/client1.png" alt="client">
          <img src="<?/*= get_template_directory_uri() */?>/images/client2.png" alt="client">
          <img src="<?/*= get_template_directory_uri() */?>/images/client3.png" alt="client">
          <img src="<?/*= get_template_directory_uri() */?>/images/client4.png" alt="client">
          <img src="<?/*= get_template_directory_uri() */?>/images/client5.png" alt="client">
          <img src="<?/*= get_template_directory_uri() */?>/images/client6.png" alt="client">
        </div>-->
      </div>

    </div>
  </div>

</main>
<?php get_footer(); ?>
