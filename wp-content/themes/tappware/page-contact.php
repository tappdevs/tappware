<?php /*Template name: Contact*/ ?>
<?php get_header(); ?>
<div class="content-heading py-3 bg-light">
  <div class="container">
    <div class="d-flex justify-content-between align-items-center">
      <h1><?php the_title(); ?></h1>
      <?php yoast_breadcrumb('<div id="breadcrumbs">', '</div>'); ?>
      <!-- /.breadcrumb -->
    </div>
    <!-- /.d-flex -->
  </div>
  <!-- /.container -->
</div>
<main>
  <div class="address-map">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3068.8649081372546!2d90.36634652010778!3d23.83580464685778!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x48cf3c37f7488f75!2sTappware+Solutions+Ltd.!5e0!3m2!1sen!2sbd!4v1563444213251!5m2!1sen!2sbd"
        width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>

  <div class="container py-5">
    <div class="row">
      <div class="col-sm-9">

        <h4 class="content-title"><span>Send us email</span></h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci dignissimos ea iusto praesentium quod
          sint. Autem consequuntur ea hic ipsum magni neque numquam perferendis porro ratione, sapiente tenetur vitae
          voluptatum.</p>

        <div class="contact-form">
			<?= do_shortcode( '[contact-form-7 id="68" title="Contact Us"]' ); ?>
        </div>
      </div>
		<?php get_sidebar( 'contact' ); ?>
    </div>
  </div>
</main>
<?php get_footer(); ?>
