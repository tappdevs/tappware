<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	<div class="border d-flex">
		<input type="text" value="" class="border-0 w-100 px-4 py-3" name="s" id="s" placeholder="Type to search ..." />
		<input type="submit" id="searchsubmit" class="fa btn btn-transparent px-4" value="" />
	</div>
</form>
